import initialState from './initialState';

export default function mainReducer(state = initialState.mainInfo, action) {
  switch(action.type) {
    default: return state;
  }
}
